﻿const USUARIO = 'jwildemer';
const API_URL = 'https://codestats.net/api/users/' + USUARIO;
const ACTUALIZACION_BREVE = 10000;
const ACTUALIZACION_LARGA = 30000;

/* SONIDO */
createjs.Sound.registerSound("sounds/SinTítulo_0-región-004.wav", "data");
createjs.Sound.registerSound("sounds/SinTítulo_0-región-001-b.wav", "data2");

export var consultarAPI = () => {
  fetch(API_URL).then(response => response.json())
    .then(apiJson => codeStats.procesarDatos(apiJson));
}
class CodeStats {
  constructor() {
    this.dato = "";
    this.lenguaje = {};
    this.fecha = {};
    this.maquinas = {};
    this.nuevaXP = 0;
    this.totalXP = 0;
  }

  procesarDatos(dato) {
    // Comprueba si hay datos nuevos
    if (this.dato != JSON.stringify(dato)) {
      this.dato = JSON.stringify(dato);
      this.lenguaje = dato["language"];
      this.fecha = dato["dates"];
      this.maquinas = dato["machines"];
      this.nuevaXP = dato["new_xp"];
      this.totalXP = dato["total_xp"];
      this.lenguaje = dato["languages"];
      setTimeout(function () { consultarAPI() }, ACTUALIZACION_BREVE); // Si hay cambios, actualiza en 10s
    }
    else {
      setTimeout(function () { consultarAPI() }, ACTUALIZACION_LARGA); // Si no hay cambios, actualiza en 30s
    }

    let statsVisuales = new EstadisticasVisuales();

    statsVisuales.showTOP(5, this.sortFor(this.lenguaje, "xps"));

    statsVisuales.moreStats(Object.keys(this.lenguaje).length, this.sortFor(this.lenguaje, "xps"));
    showStatsChart(this.sortFor(this.lenguaje, "new_xps"));
  }

  getPercentLevel(xp) {
    /* Fórmula para obtener el porcentaje total obtenido para el siguiente
    FLOOR.MATH(((0.025*RAIZ(XPS))-LVL)*100) */
    return Math.floor(((0.025 * Math.sqrt(xp)) - this.getLevel(xp)) * 100);
  }

  getLevel(xp) {
    return Math.floor(0.025 * Math.sqrt(xp));
  }

  sortFor(dict, value) {
    var items = Object.keys(dict).map(function (key) {
      return [key, dict[key][value]];
    })

    items.sort(function (primero, segundo) {
      return segundo[1] - primero[1];
    })
    return items;
  }
}

class EstadisticasVisuales {
  showTOP(number, dictLangs) {

    document.getElementById("statistics-banner").innerHTML = ``;

    for (let i = 0; i < number; i++) {
      document.getElementById("statistics-banner").innerHTML += `
      <div class="col text-center">
        <h2 id="${dictLangs[i][0]}" class="m-0">0</h2>
        <h3 class="m-0">${dictLangs[i][0]}</h3>
      </div>
      `;
    }

    // HACK: Solucion temporal para animar los contadores
    animate(document.getElementById(dictLangs[0][0]), 0, dictLangs[0][1], 3000);
    animate(document.getElementById(dictLangs[1][0]), 0, dictLangs[1][1], 2500);
    animate(document.getElementById(dictLangs[2][0]), 0, dictLangs[2][1], 2000);
    animate(document.getElementById(dictLangs[3][0]), 0, dictLangs[3][1], 1500);
    animate(document.getElementById(dictLangs[4][0]), 0, dictLangs[4][1], 1000);

    //#region CONTADOR
    function animate(obj, initVal, lastVal, duration) {
      function abbreviateNumber(value) {
        var newValue = value;
        if (value >= 1000) {
          var suffixes = ["", "k", "m", "b", "t"];
          var suffixNum = Math.floor(("" + value).length / 4);
          var shortValue = '';
          for (var precision = 2; precision >= 1; precision--) {
            shortValue = parseInt(suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value);
            var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g, '');
            if (dotLessShortValue.length <= 2) { break; }
          }
          if (shortValue % 1 != 0) shortValue = shortValue.toFixed(1);
          newValue = shortValue + suffixes[suffixNum];
        }
        return newValue;
      }
      var lastValor = null;
      let startTime = null;

      //get the current timestamp and assign it to the currentTime variable
      //let currentTime = Date.now();

      //pass the current timestamp to the step function
      const step = (currentTime) => {

        //if the start time is null, assign the current time to startTime
        if (!startTime) {
          startTime = currentTime;
        }

        //calculate the value to be used in calculating the number to be displayed
        const progress = Math.min((currentTime - startTime) / duration, 1);

        //calculate what to be displayed using the value gotten above
        obj.innerHTML = abbreviateNumber(Math.floor(progress * (lastVal - initVal) + initVal));
        if (obj.innerHTML != lastValor) {
          lastValor = obj.innerHTML;
          createjs.Sound.play("data2");
        }

        //checking to make sure the counter does not exceed the last value (lastVal)
        if (progress < 1) {
          window.requestAnimationFrame(step);
        }
        else {
          window.cancelAnimationFrame(window.requestAnimationFrame(step));
          createjs.Sound.stop();
          createjs.Sound.play("data");
        }
      };

      //start animating
      window.requestAnimationFrame(step);
    }
    //#endregion
  }

  moreStats(number, dictLangs) {
    /* Muestra en pantalla los porcentajes del nivel actual en los diferentes lenguajes */
    let columnStats = document.getElementById("columnStats");
    columnStats.innerHTML = ``;
    for (let i = 0; i < parseInt(number <= 10 ? number : 10); i++) {
      columnStats.innerHTML += `
          <div class="col-md-6 my-2">
            <div class="row justify-content-between">
              <h4 class="col text-nowrap">${dictLangs[i][0]}</h4>
              <h4 class="col text-end">Nivel: ${codeStats.getLevel(dictLangs[i][1])}</h4>
            </div>
            <div class="bar">
              <h5 id="${i}" class="skills"><label>${codeStats.getPercentLevel(dictLangs[i][1])}%</label></h5>
            </div>
          </div>
        `;
    }

    for (let i = 0; i < parseInt(number <= 10 ? number : 10); i++) {
      document.getElementById(i).style.width = `${codeStats.getPercentLevel(dictLangs[i][1])}%`;
    }
  }
}

// FEATURE: Crear un minigráfico de experiencia por día (en todo el año)
let grafico;
function showStatsChart(dictLangs) {
  let listLang = [];
  let listXP = [];

  for (let i = 0; i < dictLangs.length; i++) {
    if (dictLangs[i][1] != 0) {
      listLang.push(dictLangs[i][0]);
      listXP.push(dictLangs[i][1]);
    }
  }

  // Si hay experiencia nueva
  if (listXP.length != 0) {
    document.getElementById("chart-section").style.display = "block";

    if (grafico == null) {
      grafico = new Chart(document.getElementById("myChart").getContext('2d'), {
        type: 'horizontalBar',
        data: {
          labels: listLang,
          datasets: [{
            backgroundColor: ['#edbc1d', '#8eb53a', '#35a061', '#008578', '#006675', '#2f4858'],
            borderColor: ['#edbc1d', '#8eb53a', '#35a061', '#008578', '#006675', '#2f4858'],
            data: listXP
          }]
        },
        options: {
          responsive: true,
          legend: {
            display: false,
          },
          scales: {
            x: {
              beginAtZero: true,
            }
          },
        }
      });
    }
    else {
      grafico.config.data.datasets[0].data = listXP;
      grafico.update();
    }
  } else {
    document.getElementById("chart-section").style.display = "none";
  }
}

let codeStats = new CodeStats();