import { consultarAPI } from "./codestats.js";
import { proyectos } from "./proyectos.js";
import { certificaciones } from "./certificaciones.js";

/* START */
window.onload = () => {
  consultarAPI();
  importarProyectos();
  importarCertificaciones();
}

function importarProyectos() {
  for (let i = 0; i < proyectos.length; i++) {
    document.getElementById("listaProyectos").innerHTML += `
    <article class="col-md-4 p-5">
      <a class="card cardProject d-flex flex-column justify-content-around p-3 m-0" href="${proyectos[i].Link}" target="_blank" style="background-image: linear-gradient(rgba(0, 0, 0, var(--bg-filter-opacity)), rgba(0, 0, 0, var(--bg-filter-opacity))), url('${proyectos[i].Imagen}')">
        <h3 class="m-0">${proyectos[i].Nombre}</h3>
        <p>${proyectos[i].Descripcion}</p>
        <div class="type p-3">${proyectos[i].Tipo}</div>
        <div class="tags d-flex"></div>
      </a>
    </article>
    `;
    for (let tag of proyectos[i].Tags) {
      document.getElementsByClassName("tags")[i].innerHTML += `<div class="tag px-3 me-3"><label for="tag">${tag}</label></div>`
    }
  }
}

function importarCertificaciones() {
  for (let i = 0; i < certificaciones.length; i++) {
    document.getElementById("certificaciones").innerHTML += `
    <article class="col-md-4 p-5">
      <a class="cardCertificado d-flex flex-column justify-content-around p-3 m-0" href="${certificaciones[i].Link}" target="_blank" style="background-image: url('${certificaciones[i].Imagen}')">
      </a>
    </article>
    `;
  }
}

$(window).scroll(function () {
  var offset = $(window).scrollTop();
  $('.navbar-fixed-top').toggleClass('opaque', offset > 150);
});