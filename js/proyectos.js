﻿export var proyectos = [
  {
    Nombre: "A.K.A.S.H.I",
    Tags: [
      "Arduino",
      "C++"
    ],
    Tipo: "Hardware",
    Descripcion: "Ergonómico teclado portátil diseñado y programado para Mercadolibre. Capaz de agilizar, facilitar y unificar las tareas de sus proyectos.",
    Link: "https://gitlab.com/jonathanhacklife/akashi",
    Imagen: "img/icons/Arbusta_s_Tech.png"
  },
  {
    Nombre: "ALT - Innovación social",
    Tags: [
      "HTML",
      "CSS",
      "JavaScript"
    ],
    Tipo: "Web",
    Descripcion: "Un programa de formación profesional basado en la experiencia. Durante dos meses entrenamos nuestras habilidades para responder a las necesidades del futuro.",
    Link: "https://www.alt-innovacionsocial.com/",
    Imagen: "https://www.alt-innovacionsocial.com/wp-content/uploads/2020/09/Alt_200x200-05.png"
  },
  {
    Nombre: "Bitácora Electrónica de Pesca Argentina",
    Tags: [
      "Flutter",
      "Dart"
    ],
    Tipo: "Mobile",
    Descripcion: "BEPA es un sistema gratuito y de código abierto que permite a los capitanes y/o pescadores utilizar sus dispositivos móviles para registrar y transmitir electrónicamente datos de las capturas y el esfuerzo pesquero, y facilita a los armadores y quienes tengan acceso, a realizar un seguimiento eficaz de la pesca a través de un sitio web.",
    Link: "https://play.google.com/store/apps/details?id=com.wwf.bep_app",
    Imagen: "https://play-lh.googleusercontent.com/-5cvNX0YZrS-UWgpkhKD6x3RViGgMHbu_QqFHak_IV5o4PtMg3wJ1EvEvq6S05Rrog=s180-rw"
  },
  {
    Nombre: "Cal Construcciones",
    Tags: [
      "HTML",
      "CSS",
      "JavaScript"
    ],
    Tipo: "Web",
    Descripcion: "Web para emprendimiento de proyectos de construcción, remodelación y ampliación para viviendas, oficinas y locales comerciales.",
    Link: "http://calconstruccion.com/",
    Imagen: "https://i.ibb.co/9H21z7G/Cal-Construccio-n.jpg"
  },
  {
    Nombre: "Codestats App",
    Tags: [
      "Flutter",
      "Dart"
    ],
    Tipo: "Mobile",
    Descripcion: "Versión mobile de Code::Stats. Este es un servicio de estadísticas para programadores. Muestre sus estadísticas a sus amigos o recruiters y compare su progreso con otros.",
    Link: "https://gitlab.com/jonathanhacklife/codestats-app",
    Imagen: "img/icons/CodeStats.svg"
  },
  {
    Nombre: "Consecuencia Ventures",
    Tags: [
      "HTML",
      "CSS",
      "JavaScript"
    ],
    Tipo: "Web",
    Descripcion: "Web para emprendimiento de fondo de inversión social con la mirada puesta en Latinoamérica a través de modelos de negocio escalables y sostenibles en el tiempo.",
    Link: "http://consecuenciaventures.com/",
    Imagen: "https://i.ibb.co/6WBcjf7/Consecuencia-logo.png"
  },
  {
    Nombre: "Hackatón Arbusta",
    Tags: [
      "HTML",
      "CSS",
      "JavaScript"
    ],
    Tipo: "Web",
    Descripcion: "Landing Page desarrollada para presentar la experiencia en la Hackatón de Arbusta en el año 2019.",
    Link: "https://hackaton.arbusta.net/",
    Imagen: "img/icons/ArbustaHackatonLogo.png"
  },
  {
    Nombre: "InventApp",
    Tags: [
      "HTML",
      "CSS",
      "JavaScript"
    ],
    Tipo: "Web",
    Descripcion: "Este generador está desarrollado en base a una lista de recursos que disponemos para luego generar una lluvia de ideas entre todos estos, permitiendonos idear resultados desde su unión.",
    Link: "https://jonathanhacklife.gitlab.io/InventApp/",
    Imagen: "img/icons/InventAppLogo.png"
  },
  {
    Nombre: "Latin Business Today",
    Tags: [
      "HTML",
      "CSS",
      "JavaScript"
    ],
    Tipo: "Web",
    Descripcion: "Blog de una red de asesores que atienden a todo tipo de empresas, incluidas las start-ups, pequeñas empresas y ejecutivas.",
    Link: "https://www.latinbusinesstoday.com/",
    Imagen: "https://www.latinbusinesstoday.com/wp-content/uploads/2021/01/LBT-main-logo.png"
  },
  {
    Nombre: "Mail Template",
    Tags: [
      "HTML",
      "CSS",
      "JavaScript"
    ],
    Tipo: "Web",
    Descripcion: "Un template minimalista el cual permite crear firmas electrónicas para correo electrónico. Originalmente hecho para uso personal. ¡Pero usted puede utilizarla si quiere!",
    Link: "https://jonathanhacklife.gitlab.io/mail-template/",
    Imagen: "img/icons/Mail.png"
  },
  {
    Nombre: "Pulse",
    Tags: [
      "Java",
      "XML"
    ],
    Tipo: "Mobile",
    Descripcion: "Una pulsera antisecuestro diseñada para alertar al 911, ambulancia o bomberos de forma fácil y rápida. En un accidente, un incendio o robo se puede pedir ayuda con solo pulsar el botón indicado.",
    Link: "http://culturageek.com.ar/argentino-creo-una-pulsera-anti-secuestro/",
    Imagen: "img/icons/PULSElogo.jpg"
  },
  {
    Nombre: "VCount",
    Tags: [
      "Flutter",
      "Dart"
    ],
    Tipo: "Mobile",
    Descripcion: "Aplicación de conteo vehicular diseñado para Arbusta LABS e YPF. Esta aplicación es una herramienta usada para hacer un recuento vehicular en zonas específicas.",
    Link: "https://gitlab.com/jonathanhacklife/vcount",
    Imagen: "img/icons/VCountLogo.png"
  },
]