<h2 align="center">My portfolio is online! ✨🌠⚡</h2>
<h3 align="center"><a href="https://jonathanhacklife.gitlab.io/profile">https://jonathanhacklife.gitlab.io/profile</a></h3>

# Hi 👋, I'm Jonathan Hacklife
### A passionate and experienced freelance software developer and web designer with a strong interest in developing projects that require conceptual and analytical thinking. A passionate and experienced freelance software developer and web designer with a strong interest in developing projects that require conceptual and analytical thinking.

<a href="https://app.daily.dev/JHacklife"><img src="https://api.daily.dev/devcards/e55b32da7af645e3981c0f2882576f88.png?r=i2u" width="400" alt="Jonathan Hacklife's Dev Card"/></a>

## Some things about me
- 💡 I developed [**PULSE**](http://culturageek.com.ar/argentino-creo-una-pulsera-anti-secuestro/). An electronic anti-kidnapping bracelet.
- 🏆 I obtained the first prize and mentions for innovation with [**Alert.AR**](http://www.argentina.gob.ar/noticias/primera-hackaton-nacional-para-ayudar-frenar-la-violencia-contra-las-mujeres-0) and [**Programá Tu Futuro**](http://www.buenosaires.gob.ar/noticias/programa-tu-futuro-cerro-el-ano-con-el-ultimo-interclubes).
- 🎤 IT Speaker: [**FITS**](https://www.fits.ong/argentina/oradores.php) (Festival de Innovación y Tecnología Social) at Centro Cultural Konex.
- 🌱 I'm currently studying **Dart**, **Python** & **JavaScript**.
- 🛠 I'm currently working on [**CodeStatsApp**](https://gitlab.com/jonathanhacklife/codestats-app).
- 💼 All my projects are available [**here**](https://gitlab.com/users/jonathanhacklife/projects)!
- 📄 Get to know my experiences on [**LinkedIn**](https://www.linkedin.com/in/jonathanhacklife/).
- ⚡ Fun fact: I'm working on a telepathy project. (*really*).
- 📆 Schedule a meeting [**with me**](https://calendly.com/jonathanhacklife/1to1)!

------------

# ¡Hola! 👋, Soy Jonathan Hacklife
### Un apasionado y experimentado desarrollador de Software y diseñador Web freelance con un gran interés en desarrollo de proyectos que requieran un pensamiento conceptual y analítico. Un apasionado y experimentado desarrollador de Software y diseñador Web freelance con un gran interés en desarrollo de proyectos que requieran un pensamiento conceptual y analítico.

## Algunas cosas sobre mi
-  💡 Desarrollé [**PULSE**](http://culturageek.com.ar/argentino-creo-una-pulsera-anti-secuestro/). Una pulsera electrónica anti-secuestro.
-  🏆 Obtuve el primer premio y menciones a la innovación con [**Alert.AR**](http://www.argentina.gob.ar/noticias/primera-hackaton-nacional-para-ayudar-frenar-la-violencia-contra-las-mujeres-0) y [**Programá Tu Futuro**](http://www.buenosaires.gob.ar/noticias/programa-tu-futuro-cerro-el-ano-con-el-ultimo-interclubes).
-  🎤 Speaker IT: [**FITS**](https://www.fits.ong/argentina/oradores.php) (Festival de Innovación y Tecnología Social) en Centro Cultural Konex.
-  🌱 Actualmente estoy estudiando **Dart**, **Python** & **JavaScript**.
-  🛠 Actualmente estoy trabajando en [**CodeStatsApp**](https://gitlab.com/jonathanhacklife/codestats-app).
-  💼 ¡Todos mis proyectos están disponibles [**aquí**](https://gitlab.com/users/jonathanhacklife/projects)!
-  📄 Conoce mis experiencias en [**LinkedIn**](https://www.linkedin.com/in/jonathanhacklife/).
-  ⚡ Dato curioso: estoy trabajando en un proyecto de telepatía. (*realmente*).
-  📆 ¡Programe una [**reunión conmigo**](https://calendly.com/jonathanhacklife/1to1)!

------------

## Connect with me / Contactame:
<p align="left">
<a href="https://www.linkedin.com/in/jonathanhacklife/?locale=en_us" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg" alt="jonathanhacklife" height="30" width="40" /></a>
<a href="https://www.hackster.io/Jonathan-Hacklife" target="blank"><img align="center" src="https://d29fhpw069ctt2.cloudfront.net/icon/image/38774/preview.svg" alt="jonathanhacklife" height="30" width="40" /></a>
<a href="https://www.codewars.com/users/jhacklife" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/hackerrank.svg" alt="jhacklife" height="30" width="40" /></a>
</p>

## Languages and Tools / Lenguajes y herramientas:
<p align="left">
<a href="https://developer.android.com" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original-wordmark.svg" alt="android" width="40" height="40"/></a>
<a href="https://www.arduino.cc/" target="_blank"><img src="https://cdn.worldvectorlogo.com/logos/arduino-1.svg" alt="arduino" width="40" height="40"/></a>
<a href="https://www.w3schools.com/cpp/" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/></a>
<a href="https://www.w3schools.com/css/" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/></a>
<a href="https://dart.dev" target="_blank"><img src="https://www.vectorlogo.zone/logos/dartlang/dartlang-icon.svg" alt="dart" width="40" height="40"/></a>
<a href="https://www.figma.com/" target="_blank"><img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/></a>
<a href="https://firebase.google.com/" target="_blank"><img src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width="40" height="40"/></a>
<a href="https://flutter.dev" target="_blank"><img src="https://www.vectorlogo.zone/logos/flutterio/flutterio-icon.svg" alt="flutter" width="40" height="40"/></a>
<a href="https://git-scm.com/" target="_blank"><img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/></a>
<a href="https://www.w3.org/html/" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/></a>
<a href="https://www.adobe.com/in/products/illustrator.html" target="_blank"><img src="https://www.vectorlogo.zone/logos/adobe_illustrator/adobe_illustrator-icon.svg" alt="illustrator" width="40" height="40"/></a>
<a href="https://www.java.com" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/></a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/></a>
<a href="https://www.photoshop.com/en" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="40" height="40"/></a>
<a href="https://www.python.org" target="_blank"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/></a>
<a href="https://www.selenium.dev" target="_blank"><img src="https://raw.githubusercontent.com/detain/svg-logos/780f25886640cef088af994181646db2f6b1a3f8/svg/selenium-logo.svg" alt="selenium" width="40" height="40"/></a>
<a href="https://unity.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/unity3d/unity3d-icon.svg" alt="unity" width="40" height="40"/></a>
<a href="https://www.adobe.com/products/xd.html" target="_blank"><img src="https://cdn.worldvectorlogo.com/logos/adobe-xd.svg" alt="xd" width="40" height="40"/></a>
</p>

## Contribute to projects / Contribuir a los proyectos:
<a href="https://cafecito.app/jonathanhacklife"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" height="50" width="210" alt="jonathanhacklife"/></a>
